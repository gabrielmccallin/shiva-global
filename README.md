# shiva global #
---

### Example application built with shiva :trident:
All behaviour and presentation in this example application is achieved with code; no markup or CSS.

---

Use this repository to get started with shiva. It contains a very simple application written in JavaScript loading the shiva library as a global.

See [https://bitbucket.org/gabrielmccallin/shiva](https://bitbucket.org/gabrielmccallin/shiva) for documentation on using shiva :trident: