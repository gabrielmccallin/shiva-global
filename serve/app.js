/// <reference path="./../typings/shiva-global.d.ts" />
(function () {
    function App() {
        var root = new shiva.Container({
            root: true
        });

        var bg = new shiva.Container({
            backgroundColor: "#333333",
            width: "100%",
            height: "100%",
        });
        root.addChild(bg);

        bg.fromTo({
            duration: 5,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            }
        });

        var line = new shiva.Container({
            height: "1px",
            backgroundColor: "#9999cc",
            width: "100%",
            position: "relative",
            top: "calc(75% + 1.6rem)",
            opacity: "0.3"
        });
        bg.addChild(line);

        line.fromTo({
            ease: shiva.Ease.EaseOut,
            duration: 5,
            fromVars: {
                transform: "translateX(100%)"
            },
            toVars: {
                transform: "translateX(5rem)"
            }
        });

        var copy = new shiva.Container({
            text: "shiva-global",
            fontSize: "1.2rem",
            fontFamily: "sans-serif",
            color: "#9999cc",
            letterSpacing: "0rem",
            position: "relative",
            top: "75%"
        });
        bg.addChild(copy);

        copy.fromTo({
            duration: 5,
            ease: shiva.Ease.EaseOut,
            fromVars: {
                transform: "translateX(0rem)",
                opacity: "0"
            },
            toVars: {
                transform: "translateX(5rem)",
                opacity: "1"
            }
        });
    }

    window.onload = function () {
        App();
    };
} ());
